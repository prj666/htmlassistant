﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class blockFunctions : MonoBehaviour
{
	public GameObject block;
	public GameObject mouse;
	public SenseInput input;
	public SpeechControl spInput;
	public static List<GameObject> blocks = new List<GameObject> ();
	private GameObject selectedBlock;
	public static string mergeTemp = "";
	public run showPage;
	public static List<GameObject> buttonsFile = new List<GameObject> ();
	public static List<GameObject> buttonsBlock = new List<GameObject> ();
	public static bool merge = false;
	public static GameObject lastBlock;
	public GameObject canvas;
	public GameObject helpCanvas;
	public bool loadBlock_flag = false;
	public bool loadBlocks_flag = false;
	public bool saveBlock_flag = false;
	public bool saveBlocks_flag = false;
	public GameObject about;
	public GameObject manual;
	public GameObject voiceCommands;
	public GameObject voiceCommandsDesc;
	public GameObject StructPanel;
	private RectTransform StructRectTransform;
	public bool VoiceActive = true;


	// Use this for initialization
	void Start ()
	{
		Debug.Log ("-- blockFunctions.Start() BEGIN --");

		block = (GameObject)Resources.Load ("Block");
		buttonsFile.Add (GameObject.Find ("Save"));
		buttonsFile.Add (GameObject.Find ("Save Folder"));
		buttonsFile.Add (GameObject.Find ("Load"));
		buttonsFile.Add (GameObject.Find ("Load Folder"));
		buttonsFile.Add (GameObject.Find ("Run"));
		buttonsFile.Add (GameObject.Find ("LoadCSS"));
		buttonsFile.Add (GameObject.Find ("LoadJS"));
		buttonsFile.Add (GameObject.Find ("css"));
		buttonsFile.Add (GameObject.Find ("js"));
		buttonsBlock.Add (GameObject.Find ("Create"));
		buttonsBlock.Add (GameObject.Find ("skeleton"));
		buttonsBlock.Add (GameObject.Find ("p"));
		buttonsBlock.Add (GameObject.Find ("table"));
		buttonsBlock.Add (GameObject.Find ("li"));
		buttonsBlock.Add (GameObject.Find ("Delete"));
		buttonsBlock.Add (GameObject.Find ("Merge Blocks"));
		canvas = GameObject.Find ("Canvas");
		helpCanvas = GameObject.Find ("HelpCanvas");
		about = GameObject.Find ("About");
		manual = GameObject.Find ("Manual");
		voiceCommandsDesc = GameObject.Find ("VoiceCommandsDesc");
		voiceCommands = GameObject.Find ("VoiceCommands");
		helpCanvas.SetActive (false);

		try {
			input.OnHandData += OnHandData;
		} catch (Exception e) {
			Debug.Log ("SenseInput Error: " + e.Message);
		}
		try {
			spInput.OnVoiceData += OnVoiceData;
		} catch (Exception e) {
			Debug.Log ("SpeechControl Error: " + e.Message);
		}

		Debug.Log ("-- blockFunctions.Start() END --");
	}

	/// <summary>
	/// Function for setting the main canvas active
	/// </summary>
	void setCanvasActive ()
	{
		canvas.SetActive (true);
	}

	/// <summary>
	/// Function for setting the main canvas inactive
	/// </summary>
	void setCanvasInactive ()
	{
		canvas.SetActive (false);
	}

	/// <summary>
	/// This function automatically resizes the blocks according to their text. Called on a keystroke.
	/// </summary>
	public void ValueChangeCheck (string newText)
	{
		string[] tempText = lastBlock.GetComponent<InputField> ().text.Split ('\n');
		int amount = tempText.Length;
		foreach (string line in tempText) {
			if (line.Length > 60)
				amount = amount + (int)(line.Length / 60);
		}
		if (amount * 15 + 20 != lastBlock.GetComponent<RectTransform> ().sizeDelta.y) {
			lastBlock.GetComponent<RectTransform> ().sizeDelta = new Vector2 (lastBlock.GetComponent<RectTransform> ().sizeDelta.x, amount * 15 + 20);
		}
	}

	/// <summary>
	/// This function saves the last block focused for merging and resizing purposes. 
	/// It also adds the block text to a temporary string if the block was focused while merging, and disables that block.
	/// </summary>
	void OnGUI ()
	{
		if (Event.current.type == EventType.MouseUp && merge) {
			for (int i = 0; i < blocks.Count; i++) {
				if (blocks [i] && blocks [i].GetComponent<InputField> ().isFocused) {
					if (mergeTemp != "")
						mergeTemp = mergeTemp + "\n" + blocks [i].GetComponent<InputField> ().text.Trim ();
					else
						mergeTemp = mergeTemp + blocks [i].GetComponent<InputField> ().text.Trim ();
					blocks [i].GetComponent<InputField> ().interactable = false;
				}
			}
		} else if (Event.current.type == EventType.MouseUp) {
			for (int i = 0; i < blocks.Count; i++) {
				if (blocks [i] && blocks [i].GetComponent<InputField> ().isFocused) {
					lastBlock = blocks [i];
				}
			}
		}

		if (loadBlock_flag) {
			loadBlock_flag = false;
			string[] extensions = { ".html", ".htm" };
			// limit file selection to html and htm files
			UniFileBrowser.use.SetFileExtensions (extensions);
			UniFileBrowser.use.OpenFileWindow (loadBlock);
			UniFileBrowser.use.SendWindowCloseMessage (setCanvasActive);
		}

		if (loadBlocks_flag) {
			loadBlocks_flag = false;
			UniFileBrowser.use.OpenFolderWindow (false, loadBlocks);
			UniFileBrowser.use.SendWindowCloseMessage (setCanvasActive);
		}

		if (saveBlock_flag) {
			saveBlock_flag = false;
			UniFileBrowser.use.SaveFileWindow (saveBlock);
			UniFileBrowser.use.SendWindowCloseMessage (setCanvasActive);
		}

		if (saveBlocks_flag) {
			saveBlocks_flag = false;
			UniFileBrowser.use.OpenFolderWindow (false, saveBlocks);
			UniFileBrowser.use.SendWindowCloseMessage (setCanvasActive);
		}
	}

	//********************************************************************************************************
	//********************************___STRUCTURE BLOCKS___**************************************************
	//********************************************************************************************************
	/// <summary>
	/// Creates the block.
	/// </summary>
	public void createBlock ()
	{
		Debug.Log ("Clicked Create Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (canvas.transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "";
		Debug.Log (rt.sizeDelta.x + " " + rt.sizeDelta.y);
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the table block.
	/// </summary>
	public void createTableBlock ()
	{
		Debug.Log ("Clicked Create <table> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 200);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<table>\n <caption>Table Caption</caption>\n    <tr>\n        <th></th>\n        <th></th>\n    </tr>\n    <tr>\n        <td></td>\n        <td></td>\n    </tr>\n</table>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the div block.
	/// </summary>
	public void createDivBlock ()
	{
		Debug.Log ("Clicked <div> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 70);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<div>\n\n</div>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the nav block.
	/// </summary>
	public void createNavBlock ()
	{
		Debug.Log ("Clicked <nav> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 115);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<nav>\n    <a href=\"link here\">Main</a> |\n    <a href=\"link here\">Another Page</a> | \n    <a href=\"link here\">Another Page</a> | \n    <a href=\"link here\">Another Page</a> \n </nav>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the form block.
	/// </summary>
	public void createFormBlock ()
	{
		Debug.Log ("Clicked <form> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 200);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<form action=\"link to a script\" method=\"get\">\n  Name: <input type=\"text\" name=\"name\"><br><br>\n  Sex: <input type=\"radio\" name=\"sex\" value=\"male\" checked>Male\n  <input type=\"radio\" name=\"sex\" value=\"female\">Female<br><br>\n  Spoken Languages:<br><br> <input type=\"checkbox\" name=\"language1\" value=\"English\"> English\n  <input type=\"checkbox\" name=\"language2\" value=\"Russian\"> Russian<br><br>\n <textarea name=\"comments\" rows=\"10\" cols=\"60\">Leave a comment...</textarea><br><br>\n <input type=\"submit\" value=\"Submit\">\n</form>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the script block.
	/// </summary>
	public void createScriptBlock ()
	{
		Debug.Log ("Clicked <script> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<script>  </script>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the style block.
	/// </summary>
	public void createStyleBlock ()
	{
		Debug.Log ("Clicked <style> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<style>  </style>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the section block.
	/// </summary>
	public void createSectionBlock ()
	{
		Debug.Log ("Clicked <section> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<section>  </section>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the article block.
	/// </summary>
	public void createArticleBlock ()
	{
		Debug.Log ("Clicked <article> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<article>  </article>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the header block.
	/// </summary>
	public void createHeaderBlock ()
	{
		Debug.Log ("Clicked <header> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<header>  </header>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the footer block.
	/// </summary>
	public void createFooterBlock ()
	{
		Debug.Log ("Clicked <footer> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<footer>  </footer>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the canvas block.
	/// </summary>
	public void createCanvasBlock ()
	{
		Debug.Log ("Clicked <canvas> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 75);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<canvas id=\"myCanvas\" width=\"200\" height=\"100\" style=\"border:4px solid #000000;\">\n  \n</canvas>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	//********************************************************************************************************
	//********************************___LISTS BLOCKS___*******************************************************
	//********************************************************************************************************
	/// <summary>
	/// Creates the ul block.
	/// </summary>
	public void createUlBlock ()
	{
		Debug.Log ("Clicked Create <ul> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 100);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<ul>\n    <li></li>\n    <li></li>\n    <li></li>\n</ul>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the ol block.
	/// </summary>
	public void createOlBlock ()
	{
		Debug.Log ("Clicked Create <ol> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 100);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<ol>\n    <li></li>\n    <li></li>\n    <li></li>\n</ol>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the DL block.
	/// </summary>
	public void createDLBlock ()
	{
		Debug.Log ("Clicked Create <dl> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 115);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<dl>\n  <dt>Item</dt>\n  <dd>Item definition</dd>\n  <dt>Item</dt>\n  <dd>Item definition</dd>\n</dl>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the select list block.
	/// </summary>
	public void createSelectListBlock ()
	{
		Debug.Log ("Clicked Create <select> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 180);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<select>\n  <optgroup label=\"Category\">\n    <option value=\"1\">Item 1</option>\n    <option value=\"2\">Item 2</option>\n  </optgroup>\n  <optgroup label=\"Category 2\">\n    <option value=\"3\">Item 1</option>\n    <option value=\"4\">Item 2</option>\n  </optgroup>\n</select>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the data list block.
	/// </summary>
	public void createDataListBlock ()
	{
		Debug.Log ("Clicked Create <datalist> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 200);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<form action=\"link to script\" method=\"get\">\n<input list=\"Items\" name=\"item\">\n<datalist id=\"items\">\n<option value=\"Item 1\">\n<option value=\"Item 2\">\n<option value=\"Item 3\">\n<option value=\"Item 4\">\n<option value=\"Item 5\">\n</datalist>\n<input type=\"submit\">\n</form>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	//********************************************************************************************************
	//********************************___TEXT BLOCKS___*******************************************************
	//********************************************************************************************************
	/// <summary>
	/// Creates the P block.
	/// </summary>
	public void createPBlock ()
	{
		Debug.Log ("Clicked Create <p> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<p> Your paragraph here </p>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the PRE block.
	/// </summary>
	public void createPREBlock ()
	{
		Debug.Log ("Clicked Create <pre> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<pre> Your preformatted text here </pre>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the h1 block.
	/// </summary>
	public void createH1Block ()
	{
		Debug.Log ("Clicked Create <h1> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		;
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<h1> Largest Heading. Change from 1-6 </h1>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the italics block.
	/// </summary>
	public void createItalicsBlock ()
	{
		Debug.Log ("Clicked Create <i> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<i> Your Italics Text here </i>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the bold block.
	/// </summary>
	public void createBoldBlock ()
	{
		Debug.Log ("Clicked Create <b> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<b> Your Bold Text here </b>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the block quote block.
	/// </summary>
	public void createBlockQuoteBlock ()
	{
		Debug.Log ("Clicked Create <blockquote> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<blockquote cite=\"url here\"> quote here </blockquote>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the hyper link block.
	/// </summary>
	public void createHyperLinkBlock ()
	{
		Debug.Log ("Clicked Create <blockquote> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<a href=\"put your url here\"> Text to click on </a>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	//********************************************************************************************************
	//********************************___MEDIA BLOCKS___*******************************************************
	//********************************************************************************************************
	/// <summary>
	/// Creates the figure block.
	/// </summary>
	public void createFigureBlock ()
	{
		Debug.Log ("Clicked Create <figure> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 90);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<figure>\n  <img src=\"link_to_image.jpg\" alt=\"Image name\" width=\"304\" height=\"228\">\n<figcaption>Fig1. - Image description.</figcaption>\n</figure>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the embed block.
	/// </summary>
	public void createEmbedBlock ()
	{
		Debug.Log ("Clicked Create <embed> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<embed src=\"link to embed content\" width=\"200\" height=\"200\">";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the image block.
	/// </summary>
	public void createImageBlock ()
	{
		Debug.Log ("Clicked Create <img> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 35);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<img src=\"link_to_image.gif\" alt=\"Image name\" height=\"42\" width=\"42\">";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the audio block.
	/// </summary>
	public void createAudioBlock ()
	{
		Debug.Log ("Clicked Create <audio> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 105);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<audio controls>\n<source src=\"audiofile.ogg\" type=\"audio/ogg\">\n<source src=\"audiofile.mp3\" type=\"audio/mpeg\">\nYour browser does not support the audio element.\n</audio>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Creates the video block.
	/// </summary>
	public void createVideoBlock ()
	{
		Debug.Log ("Clicked Create <video> Block button");
		GameObject newBlock = Instantiate (block);
		newBlock.transform.SetParent (GameObject.Find ("Canvas").transform, false);
		RectTransform rt = newBlock.GetComponent<RectTransform> ();
		rt.sizeDelta = new Vector2 (rt.sizeDelta.x, 105);
		InputField inf = newBlock.GetComponent<InputField> ();
		inf.text = "<video width=\"320\" height=\"240\" controls>\n<source src=\"movie.mp4\" type=\"video/mp4\">\n<source src=\"movie.ogg\" type=\"video/ogg\">\nYour browser does not support the video tag.\n</video>";
		inf.onValueChange.AddListener (ValueChangeCheck);
		blocks.Add (newBlock);
	}

	/// <summary>
	/// Deactivate main canvas and set flag for OnGUI to open the file browser and allow the user to select a file/save location, which will be passed to the saveBlock function
	/// </summary>
	public void runSaveBlock ()
	{
		Debug.Log ("Clicked Save button");
		canvas.SetActive (false);
		saveBlock_flag = true;
	}

	/// <summary>
	/// This functions searched for blocks inside <head> area, adds their text to the save file, and does the same for the body block.
	/// </summary>
	public void saveBlock (string htmlFile)
	{
		Debug.Log ("Clicked Save button");
		string result = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\" /><title>Try</title><head>";
		SortedList fieldList = new SortedList ();
		GameObject panel = GameObject.Find ("PanelHead");
		RectTransform rt = panel.GetComponent<RectTransform> ();
		Vector3 vectorPanel = panel.transform.position;
		for (int i = 0; i < blockFunctions.blocks.Count; i++) {
			if (blockFunctions.blocks [i]) {
				InputField temp = blockFunctions.blocks [i].GetComponent<InputField> ();
				Vector3 vectorTemp = temp.transform.position;
				if (((vectorTemp.x < (vectorPanel.x + 0.5 * rt.sizeDelta.x)) && (vectorTemp.x > (vectorPanel.x - 0.5 * rt.sizeDelta.x))) && ((vectorTemp.y < (vectorPanel.y + 0.5 * rt.sizeDelta.y)) && (vectorTemp.y > (vectorPanel.y - 0.5 * rt.sizeDelta.y))))
					fieldList.Add (vectorTemp.y, temp.text);
			}
		}
		for (int i = fieldList.Count - 1; i >= 0; i--) {
			result = result + (fieldList.GetByIndex (i)) + "\n";
		}
		fieldList.Clear ();
		result = result + "</head><body>";
		panel = GameObject.Find ("PanelBody");
		rt = panel.GetComponent<RectTransform> ();
		vectorPanel = panel.transform.position;
		for (int i = 0; i < blockFunctions.blocks.Count; i++) {
			if (blockFunctions.blocks [i]) {
				InputField temp = blockFunctions.blocks [i].GetComponent<InputField> ();
				Vector3 vectorTemp = temp.transform.position;
				if (((vectorTemp.x < (vectorPanel.x + 0.5 * rt.sizeDelta.x)) && (vectorTemp.x > (vectorPanel.x - 0.5 * rt.sizeDelta.x))) && ((vectorTemp.y < (vectorPanel.y + 0.5 * rt.sizeDelta.y)) && (vectorTemp.y > (vectorPanel.y - 0.5 * rt.sizeDelta.y))))
					fieldList.Add (vectorTemp.y, temp.text);
			}
		}
		for (int i = fieldList.Count - 1; i >= 0; i--) {
			result = result + (fieldList.GetByIndex (i)) + "\n";
		}
		fieldList.Clear ();
		result = result + "</body></html>";
		File.WriteAllText (htmlFile, result);
	}

	/// <summary>
	/// Deactivate main canvas and set flag for OnGUI to open the file browser and allow the user to select a folder to save to, which will be passed to the saveBlocks function
	/// </summary>
	public void runSaveBlocks ()
	{
		Debug.Log ("Clicked Save Folder button");
		canvas.SetActive (false);
		saveBlocks_flag = true;
	}

	/// <summary>
	/// This functions searched for all blocks on the screen and saves them in separate files.
	/// </summary>
	public void saveBlocks (string folder)
	{
		Debug.Log ("Clicked Save button");
		SortedList fieldList = new SortedList ();
		for (int i = 0; i < blocks.Count; i++) {
			if (blocks [i]) {
				InputField temp = blocks [i].GetComponent<InputField> ();
				Vector3 vectorTemp = temp.transform.position;
				Text t = temp.placeholder.GetComponent<Text> ();
				if (t.name != "js" && t.name != "css")
					fieldList.Add (vectorTemp.y, temp.text);
			}
		}
		for (int i = fieldList.Count; i > 0; i--) {
			File.WriteAllText (folder + "\\Block_" + i + ".html", (string)fieldList.GetByIndex (i - 1));
		}
	}

	/// <summary>
	/// Deactivate main canvas and set flag for OnGUI to open the file browser and allow the user to select a file to load, which will be passed to the loadBlock function
	/// </summary>
	public void runLoadBlock ()
	{
		Debug.Log ("Clicked Load button");
		canvas.SetActive (false);
		loadBlock_flag = true;
	}

	/// <summary>
	/// This functions creates a new block and loads the content of the chosen file as the block text.
	/// </summary>
	public void loadBlock (string htmlFile)
	{
		Debug.Log ("Clicked Load button");
		try {
			GameObject newBlock = Instantiate (block);
			newBlock.transform.SetParent (canvas.transform, false);
			InputField inf = newBlock.GetComponent<InputField> ();
			inf.text = File.ReadAllText (htmlFile);
			RectTransform rt = newBlock.GetComponent<RectTransform> ();
			string[] tempText = inf.text.Split ('\n');
			int amount = tempText.Length;
			foreach (string line in tempText) {
				if (line.Length > 45)
					amount = amount + (int)(line.Length / 45);
			}
			rt.sizeDelta = new Vector2 (rt.sizeDelta.x, amount * 15 + 20);
			inf.onValueChange.AddListener (ValueChangeCheck);
			blocks.Add (newBlock);
		} catch (Exception e) {
			Debug.Log ("Error: " + e.Message);
		}
	}

	/// <summary>
	/// Deactivate main canvas and set flag for OnGUI to open the file browser and allow the user to select a folder to load, which will be passed to the loadBlocks function
	/// </summary>
	public void runLoadBlocks ()
	{
		Debug.Log ("Clicked Load Folder button");
		canvas.SetActive (false);
		loadBlocks_flag = true;
	}

	/// <summary>
	/// This functions creates one block per file in the chosen folder and loads file content as the block text.
	/// </summary>
	public void loadBlocks (string folder)
	{
		Debug.Log ("Clicked Load button");
		try {
			Debug.Log ("folder = " + folder);
			string[] files = Directory.GetFiles (folder);
			foreach (string file in files) {
				if (file.EndsWith (".html")) {
					GameObject newBlock = Instantiate (block);
					newBlock.transform.SetParent (canvas.transform, false);
					InputField inf = newBlock.GetComponent<InputField> ();
					inf.text = File.ReadAllText (file);
					RectTransform rt = newBlock.GetComponent<RectTransform> ();
					string[] tempText = inf.text.Split ('\n');
					int amount = tempText.Length;
					foreach (string line in tempText) {
						if (line.Length > 45)
							amount = amount + (int)(line.Length / 45);
					}
					rt.sizeDelta = new Vector2 (rt.sizeDelta.x, amount * 15 + 20);
					inf.onValueChange.AddListener (ValueChangeCheck);
					blocks.Add (newBlock);
				}
			}
		} catch (Exception e) {
			Debug.Log ("Error: " + e.Message);
		}
	}

	/// <summary>
	/// This functions activates and deactivates Merge mode, deletes deactivated during the merge blocks, and creates new block with the content of the merged blocks.
	/// </summary>
	public void mergeBlock ()
	{
		Debug.Log ("Clicked Merge button");

		if (merge && mergeTemp != "") {
			GameObject newBlock = Instantiate (block);
			newBlock.transform.SetParent (canvas.transform, false);
			RectTransform rt = newBlock.GetComponent<RectTransform> ();
			string[] tempText = mergeTemp.Split ('\n');
			int amount = tempText.Length;
			foreach (string line in tempText) {
				if (line.Length > 45)
					amount = amount + (int)(line.Length / 45);
			}
			rt.sizeDelta = new Vector2 (rt.sizeDelta.x, amount * 15 + 20);
			InputField inf = newBlock.GetComponent<InputField> ();
			inf.text = mergeTemp;
			inf.onValueChange.AddListener (ValueChangeCheck);
			blocks.Add (newBlock);
			for (int i = 0; i < blocks.Count; i++) {
				if (blocks [i] && !blocks [i].GetComponent<InputField> ().interactable) {
					//blocks.Remove(blocks[i]);
					Destroy (blocks [i]);

				}
			}
			merge = false;
			mergeTemp = "";
			GameObject.Find ("Merge Blocks").GetComponentInChildren<Text> ().text = "Merge Blocks";
		} else if (merge) {
			merge = false;
			GameObject.Find ("Merge Blocks").GetComponentInChildren<Text> ().text = "Merge Blocks";
		} else {
			merge = true;
			GameObject.Find ("Merge Blocks").GetComponentInChildren<Text> ().text = "Finish Merging";
		}
	}

	/// <summary>
	/// This functions deletes blocks, located in the Delete area.
	/// </summary>
	public void deleteBlock ()
	{
		Debug.Log ("Clicked Delete button");
		GameObject panel = GameObject.Find ("DeleteP");
		RectTransform rt = panel.GetComponent<RectTransform> ();
		for (int i = 0; i < blocks.Count; i++) {
			if (blocks [i]) {
				Vector3 vectorTemp = blocks [i].transform.position;
				Vector3 vectorPanel = panel.transform.position;
				if (((vectorTemp.x < (vectorPanel.x + 0.5 * rt.sizeDelta.x)) && (vectorTemp.x > (vectorPanel.x - 0.5 * rt.sizeDelta.x))) && ((vectorTemp.y < (vectorPanel.y + 0.5 * rt.sizeDelta.y)) && (vectorTemp.y > (vectorPanel.y - 0.5 * rt.sizeDelta.y)))) {
					Destroy (blocks [i]);
					blocks.Remove (blocks [i]);
				}
			}
		}
	}

	/// <summary>
	/// Files the menu.
	/// </summary>
	public void fileMenu ()
	{
		Debug.Log ("Clicked File Menu button");

		GameObject.Find ("FILEButton").SetActive (true);        
	} 
    
	/// <summary>
	/// This functions deletes all created blocks on the screen.
	/// </summary>
	public void newScreen ()
	{
		Debug.Log ("Clicked New button");
		
		for (int i = 0; i < blocks.Count; i++) {
			if (blocks [i]) {
				Destroy (blocks [i]);
				blocks.Remove (blocks [i]);
			}
		}		
	} 
    
	/// <summary>
	/// Display the about screen
	/// </summary>
	public void showAbout ()
	{
		canvas.SetActive (false);
		helpCanvas.SetActive (true);
		manual.SetActive (false);
		voiceCommands.SetActive (false);
		voiceCommandsDesc.SetActive (false);
		about.SetActive (true);
	}

	/// <summary>
	/// Display the user manual screen
	/// </summary>
	public void showManual ()
	{
		canvas.SetActive (false);
		helpCanvas.SetActive (true);
		about.SetActive (false);
		voiceCommands.SetActive (false);
		voiceCommandsDesc.SetActive (false);
		manual.SetActive (true);
	}

	/// <summary>
	/// Display the voice commands screen
	/// </summary>
	public void showVoiceCommands ()
	{
		canvas.SetActive (false);
		helpCanvas.SetActive (true);
		about.SetActive (false);
		manual.SetActive (false);
		voiceCommands.SetActive (true);
		voiceCommandsDesc.SetActive (true);
	}

	/// <summary>
	/// Close any screens open from the help menu
	/// </summary>
	public void closeHelp ()
	{
		helpCanvas.SetActive (false);
		canvas.SetActive (true);
		about.SetActive (true);
		manual.SetActive (true);
		voiceCommands.SetActive (true);
		voiceCommandsDesc.SetActive (true);
	}

	/// <summary>
	/// Opens the block menu
	/// </summary>
	public void blockMenu ()
	{
		Debug.Log ("Clicked Block Menu button");
		
		Toggle t = GameObject.Find ("Block Menu").GetComponent<Toggle> ();
		if (t.isOn) {
			for (int i = 0; i < buttonsFile.Count; i++) {
				buttonsBlock [i].SetActive (true);
			}
		} else {
			for (int i = 0; i < buttonsFile.Count; i++) {
				buttonsBlock [i].SetActive (false);
			}
		}
	}

	/// <summary>
	/// Exit the application.
	/// </summary>
	public void Exit ()
	{
		Application.Quit ();
	}

	/// <summary>
	/// Moves the selected block
	/// </summary>
	/// <param name="handPosition">The co-ordinates of the hand on the screen.</param>
	public void MoveBlock (Vector3 handPosition)
	{
		//Transform the block to move to location of hand
		if (selectedBlock != null) {
			selectedBlock.transform.position = handPosition;
		}
	}

	/// <summary>
	/// Gets the size of the passed in object.
	/// </summary>
	Vector3 GetObjectSize (GameObject obj)
	{
		RectTransform rt = obj.GetComponent<RectTransform> ();
		return new Vector3 ((rt.sizeDelta.x / 2), (rt.sizeDelta.y / 2));
	}

	//
	/// <summary>
	/// Gets the cursor position adjusted for screen size and offset
	/// </summary>
	Vector3 GetCursorPosition (Vector3 origin, Vector3 offset)
	{
		origin.x = origin.x > (Screen.width - offset.x) ? (Screen.width - offset.x) 
			: origin.x < offset.x ? offset.x : origin.x;
		origin.y = origin.y > (Screen.height - offset.y) ? (Screen.height - offset.y) 
			: origin.y < offset.y ? offset.y : origin.y;
		origin.z = -100;

		return origin;
	}

	/// <summary>
	/// Raises the hand data event.
	/// </summary>
	void OnHandData (Vector3 xy)
	{ 
		//Invert the hand movement-moving hand up moves co-ordinates down without this
		xy.y = Screen.height - xy.y;

		//Move the cursor
		mouse.transform.position = GetCursorPosition (xy, GetObjectSize (mouse));

		if (input.moveMode) {
			if (selectedBlock == null) {
				foreach (GameObject block in blocks) {
					Vector3 size = GetObjectSize (block);
					
					Vector3 blockcenter = block.transform.position;
					if (xy.x < (blockcenter.x + size.x) && xy.x > (blockcenter.x - size.x) &&
						xy.y < (blockcenter.y + size.y) && xy.y > (blockcenter.y - size.y))
						selectedBlock = block;
				}
			}
			if (selectedBlock != null)
				selectedBlock.transform.position = GetCursorPosition (xy, GetObjectSize (selectedBlock));
		} else
			selectedBlock = null;
	}

	/// <summary>
	/// This function is changing VoiceActive variable and activate or deactivate voice commands by changing VoiceActive either to TRUE or FALSE.
	/// </summary>
	public void ChangeVoiceInputMode ()
	{

		if (VoiceActive) {

			VoiceActive = false;
			GameObject.Find ("Voice Commands").GetComponentInChildren<Text> ().text = "Voice Commands: Off";
		} else {
			VoiceActive = true;
			GameObject.Find ("Voice Commands").GetComponentInChildren<Text> ().text = "Voice Commands: On";
		}
	}

	/// <summary>
	/// This function takes string of characters as a parameter. This string is a recognized speech passed from Update() function from SpeechControl.cs. Depending on 
	/// the on the contents of the string, this function make funtion calls to other functions that correspond to specific voice command. Mostly, voice commands related to creating of blocks of HTML code. 
	/// VoiceActive is a global boolean variable that defines if voice commands are active or not. If VoiceActive is FALSE then no command will
	/// call any function, except ones that activate voice commands, meaning setting VoiceActive to TRUE;
	/// </summary>
	void OnVoiceData (PXCMSpeechRecognition.RecognitionData data)
	{
		if ((data.scores [0].sentence == "Enable" || data.scores [0].sentence == "Activate" || data.scores [0].sentence == "On") & VoiceActive == false) {
			ChangeVoiceInputMode ();
			UnityEngine.Debug.Log ("Call Activate Voice Function");
		}
		if ((data.scores [0].sentence == "Disable" || data.scores [0].sentence == "Deactivate" || data.scores [0].sentence == "OFF") & VoiceActive == true) {
			ChangeVoiceInputMode ();
			UnityEngine.Debug.Log ("Call Deactivate Voice Function");
		}
		if (VoiceActive) {

			//////////////////////////////////////////////BLOCKS CREATING///////////////////////////////////////////////////////////
			if (data.scores [0].sentence == "Create" || data.scores [0].sentence == "Great" || data.scores [0].sentence == "New" ||
				data.scores [0].sentence == "You" || data.scores [0].sentence == "Block" || data.scores [0].sentence == "Knew") {
				createBlock ();
				UnityEngine.Debug.Log ("Call Create Function");
			}
			if (data.scores [0].sentence == "Save" || data.scores [0].sentence == "Safe" || data.scores [0].sentence == "Say" || data.scores [0].sentence == "See") {
				runSaveBlock ();
				UnityEngine.Debug.Log ("Call Save Function");
			}
			if (data.scores [0].sentence == "Save all" || data.scores [0].sentence == "Safe all" || data.scores [0].sentence == "Say all" || data.scores [0].sentence == "See all") {
				runSaveBlocks ();
				UnityEngine.Debug.Log ("Call Save All Function");
			}
			if (data.scores [0].sentence == "Load" || data.scores [0].sentence == "Low" || data.scores [0].sentence == "Law" || data.scores [0].sentence == "Existing" || data.scores [0].sentence == "Keep") {
				runLoadBlock ();
				UnityEngine.Debug.Log ("Call Load Function");
			}
			if (data.scores [0].sentence == "Load folder" || data.scores [0].sentence == "Load all" || data.scores [0].sentence == "Lord Folder" || data.scores [0].sentence == "Low all") {
				runLoadBlocks ();
				UnityEngine.Debug.Log ("Call Load Folder Function");
			}
			if (data.scores [0].sentence == "Run" || data.scores [0].sentence == "Start" || data.scores [0].sentence == "Show") {
				showPage.renderHTML ();
				UnityEngine.Debug.Log ("Call Run Function");
			}
			if (data.scores [0].sentence == "Delete" || data.scores [0].sentence == "Erase" || data.scores [0].sentence == "You race") {
				deleteBlock ();
				UnityEngine.Debug.Log ("Call Delete Function");
			}
			if (data.scores [0].sentence == "Delete all" || data.scores [0].sentence == "Clear") {
				newScreen ();
				UnityEngine.Debug.Log ("Call Delete Function");
			}
			if (data.scores [0].sentence == "Table" || data.scores [0].sentence == "Matrix") {
				createTableBlock ();
				UnityEngine.Debug.Log ("Call Table Function");
			}
			//if (data.scores[0].sentence == "Menu" || data.scores[0].sentence == "File")
			//{
			//    fileMenu();
			//    UnityEngine.Debug.Log("Call Menu Function");
			//}
			if (data.scores [0].sentence == "Image" || data.scores [0].sentence == "Picture") {
				createImageBlock ();
				UnityEngine.Debug.Log ("Call Image block Function");
			}
			if (data.scores [0].sentence == "Audio" || data.scores [0].sentence == "Sound" || data.scores [0].sentence == "Song" || data.scores [0].sentence == "How are you") {
				createAudioBlock ();
				UnityEngine.Debug.Log ("Call Audio Function");
			}
			if (data.scores [0].sentence == "Video" || data.scores [0].sentence == "Movie" || data.scores [0].sentence == "Clip") {
				createVideoBlock ();
				UnityEngine.Debug.Log ("Call Video Function");
			}
			if (data.scores [0].sentence == "Figure" || data.scores [0].sentence == "Caption") {
				createFigureBlock ();
				UnityEngine.Debug.Log ("Call Figure Function");
			}
			if (data.scores [0].sentence == "Merge" || data.scores [0].sentence == "Marge" || data.scores [0].sentence == "Marriage" || data.scores [0].sentence == "Join") {
				mergeBlock ();
				UnityEngine.Debug.Log ("Call Merge Function");
			}
			if (data.scores [0].sentence == "Finish" || data.scores [0].sentence == "Done") {
				mergeBlock ();
				UnityEngine.Debug.Log ("Call Finish Merge Function");
			}
			if (data.scores [0].sentence == "Exit" || data.scores [0].sentence == "Quit") {
				Exit ();
				UnityEngine.Debug.Log ("Call Exit Function");
			}
			if (data.scores [0].sentence == "Insert") {
				UnityEngine.Debug.Log ("Call Embed Function");
				createEmbedBlock ();

			}
			if (data.scores [0].sentence == "Division") {
				createDivBlock ();
				UnityEngine.Debug.Log ("Call Div Function");
			}
			if (data.scores [0].sentence == "Navigation" || data.scores [0].sentence == "Menu") {
				createNavBlock ();
				UnityEngine.Debug.Log ("Call Nav Function");
			}
			if (data.scores [0].sentence == "Form" || data.scores [0].sentence == "From") {
				createFormBlock ();
				UnityEngine.Debug.Log ("Call Form Function");
			}
			if (data.scores [0].sentence == "Script" || data.scores [0].sentence == "Code") {
				createScriptBlock ();
				UnityEngine.Debug.Log ("Call Nav Function");
			}
			if (data.scores [0].sentence == "Paragraph" || data.scores [0].sentence == "Text" || data.scores [0].sentence == "Telegraph") {
				createPBlock ();
				UnityEngine.Debug.Log ("Call P Function");
			}
			if (data.scores [0].sentence == "Preformatted" || data.scores [0].sentence == "Formatted"
				|| data.scores [0].sentence == "Reformat" || data.scores [0].sentence == "Format") {
				createPREBlock ();
				UnityEngine.Debug.Log ("Call PRE Function");
			}
			if (data.scores [0].sentence == "Heading" || data.scores [0].sentence == "Title") {
				createH1Block ();
				UnityEngine.Debug.Log ("Call H1 Function");
			}
			if (data.scores [0].sentence == "Italics" || data.scores [0].sentence == "Alex" ||
				data.scores [0].sentence == "Cursive" || data.scores [0].sentence == "Cursive") {
				createItalicsBlock ();
				UnityEngine.Debug.Log ("Call Italics Function");
			}
			if (data.scores [0].sentence == "Bold" || data.scores [0].sentence == "Bald" || data.scores [0].sentence == "Ball" || data.scores [0].sentence == "Fat") {
				createBoldBlock ();
				UnityEngine.Debug.Log ("Call Bold Function");
			}
			if (data.scores [0].sentence == "Quote" || data.scores [0].sentence == "BlockQuote" || data.scores [0].sentence == "\"") {
				createBlockQuoteBlock ();
				UnityEngine.Debug.Log ("Call Quote Function");
			}
			if (data.scores [0].sentence == "Link" || data.scores [0].sentence == "Hyperlink") {
				createHyperLinkBlock ();
				UnityEngine.Debug.Log ("Call HyperLink Function");
			}
			if (data.scores [0].sentence == "OL" || data.scores [0].sentence == "Ordered list" || data.scores [0].sentence == "Ordered") {
				createOlBlock ();
				UnityEngine.Debug.Log ("Call Ol Function");
			}
			if (data.scores [0].sentence == "UL" || data.scores [0].sentence == "Unordered list" || data.scores [0].sentence == "Unordered") {
				createUlBlock ();
				UnityEngine.Debug.Log ("Call Ul Function");
			}
			if (data.scores [0].sentence == "DL" || data.scores [0].sentence == "Detail list" || data.scores [0].sentence == "Detail" || data.scores [0].sentence == "Detail least") {
				createDLBlock ();
				UnityEngine.Debug.Log ("Call Dl Function");
			}
			if (data.scores [0].sentence == "Select" || data.scores [0].sentence == "Select list" || data.scores [0].sentence == "Select least") {
				createSelectListBlock ();
				UnityEngine.Debug.Log ("Call Select List Function");
			}
			if (data.scores [0].sentence == "Data" || data.scores [0].sentence == "Data list" || data.scores [0].sentence == "Data least" || data.scores [0].sentence == "Beta") {
				createDataListBlock ();
				UnityEngine.Debug.Log ("Call Data List Function");
			}
			if (data.scores [0].sentence == "Article") {
				createArticleBlock ();
				UnityEngine.Debug.Log ("Call Article Function");
			} 
			if (data.scores [0].sentence == "Section") {
				createSectionBlock ();
				UnityEngine.Debug.Log ("Call Section Function");
			}
			if (data.scores [0].sentence == "Header" || data.scores [0].sentence == "Heather" || data.scores [0].sentence == "Have" || data.scores [0].sentence == "Head" || data.scores [0].sentence == "Harder") {
				createHeaderBlock ();
				UnityEngine.Debug.Log ("Call Header Function");
			}
			if (data.scores [0].sentence == "Footer" || data.scores [0].sentence == "Foot" || data.scores [0].sentence == "Fultron" || data.scores [0].sentence == "Food" || data.scores [0].sentence == "Foot") {
				createFooterBlock ();
				UnityEngine.Debug.Log ("Call Footer Function");
			}
			if (data.scores [0].sentence == "Canvas" || data.scores [0].sentence == "Campus" || data.scores [0].sentence == "On Bus" || data.scores [0].sentence == "Cambus" 
				|| data.scores [0].sentence == "Can" || data.scores [0].sentence == "Can Bus") {
				createCanvasBlock ();
				UnityEngine.Debug.Log ("Call Canvas Function");
			}
		}
	}
}