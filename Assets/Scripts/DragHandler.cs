﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

	public static GameObject itemBeingDragged;
	Vector3 startPosition;
	Transform startParent;
	Toggle tog;

	#region IBeginDragHandler implementation

	/// <summary>
	/// Assign the object under the cursor as the object to be dragged when left mouse button is clicked (unless the freeze blocks toggle is ON)
	/// </summary>
	public void OnBeginDrag (PointerEventData eventData)
	{
		tog = GameObject.Find ("Freeze").GetComponent<Toggle> ();
		if (!tog.isOn) {
			itemBeingDragged = gameObject;
		} else
			itemBeingDragged = null;
	}

	#endregion

	#region IDragHandler implementation

	/// <summary>
	/// Continuously change the position of the object being dragged to match the position of the mouse cursor while left mouse button is held (unless the freeze blocks toggle is ON)
	/// </summary>
	public void OnDrag (PointerEventData eventData)
	{
		tog = GameObject.Find ("Freeze").GetComponent<Toggle> ();
		if (!tog.isOn) {
			transform.position = Input.mousePosition;
		}
	}

	#endregion

	#region IEndDragHandler implementation

	/// <summary>
	/// Set the object being dragged to null when left mouse button is released (unless the freeze blocks toggle is ON)
	/// </summary>
	/// <param name="eventData">Event data.</param>
	public void OnEndDrag (PointerEventData eventData)
	{
		itemBeingDragged = null;
	}

	#endregion
}
