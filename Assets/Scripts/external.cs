﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class external : MonoBehaviour
{

	public bool getCSS = false;
	public bool getJS = false;
	public GameObject canvas;
	public string cssFile, jsFile;

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
	{
		canvas = GameObject.Find ("Canvas");
	}

	/// <summary>
	/// Function for setting the main canvas active
	/// </summary>
	void setCanvasActive ()
	{
		canvas.SetActive (true);
	}

	/// <summary>
	/// OnGUI updates the scene several times a second and repsonds to GUI events
	/// </summary>
	void OnGUI ()
	{

		// To ensure the file browser does not interfere with the main scene,
		// the main canvas is deactivated whenever the file browser is used
		// and then reactivated once the file browser is closed

		if (getCSS) {
			getCSS = false;
			string[] extensions = {".css"};
			// limit file selection to css files
			UniFileBrowser.use.SetFileExtensions (extensions);
			UniFileBrowser.use.OpenFileWindow (getCSSFile);
			UniFileBrowser.use.SendWindowCloseMessage (setCanvasActive);
		}

		if (getJS) {
			getJS = false;
			string[] extensions = {".js"};
			// limit file selection to JavaScript files
			UniFileBrowser.use.SetFileExtensions (extensions);
			UniFileBrowser.use.OpenFileWindow (getJavaScriptFile);
			UniFileBrowser.use.SendWindowCloseMessage (setCanvasActive);
		}
	}

	/// <summary>
	/// Deactivate main canvas and set flag for OnGUI to call the getCSS function
	/// </summary>
	public void openCSSFile ()
	{
		canvas.SetActive (false);
		getCSS = true;
	}

	/// <summary>
	/// Deactivate main canvas and set flag for OnGUI to call the getJS function
	/// </summary>
	public void openJSFile ()
	{
		canvas.SetActive (false);
		getJS = true;
	}

	/// <summary>
	/// Takes the path to the selected css file as a string argument and inserts it into the css pathname field in the main scene
	/// </summary>
	public void getCSSFile (string cssFile)
	{
		GameObject css_go = GameObject.Find ("css");
		InputField css_if = css_go.GetComponent<InputField> ();
		css_if.text = cssFile;
	}

	/// <summary>
	/// Takes the path to the selected JavaScript file as a string argument and inserts it into the JavaScript pathname field in the main scene
	/// </summary>
	public void getJavaScriptFile (string jsFile)
	{
		GameObject js_go = GameObject.Find ("js");
		InputField js_if = js_go.GetComponent<InputField> ();
		js_if.text = jsFile;
	}
}
