﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System;

public class run : MonoBehaviour
{

	/// <summary>
	/// Adds the CS. 
	/// </summary>
	public void addCSS ()
	{

		GameObject css_go = GameObject.Find("css");
		InputField css_if = css_go.GetComponent<InputField> ();
		string cssPath = css_if.text;
		
		if (cssPath != "") {
			
			string output = File.ReadAllText ("HTMLAssistant_Data\\output.html");
			string outputWithCSS = output.Insert (output.IndexOf ("</head>"), "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + cssPath + "\">");
			File.WriteAllText ("HTMLAssistant_Data\\output.html", outputWithCSS);
		}
	}

	/// <summary>
	/// If an external JavaScript file is loaded, insert the reference at the end of the <body> section (for faster page loading) before rendering the HTML file
	/// </summary>
	public void addJS ()
	{
		
		GameObject js_go = GameObject.Find ("js");
		InputField js_if = js_go.GetComponent<InputField> ();
		string jsPath = js_if.text;
		
		if (jsPath != "") {
			
			string output = File.ReadAllText ("HTMLAssistant_Data\\output.html");
			string outputWithJS = output.Insert (output.IndexOf ("</body>"), "<script src=\"" + jsPath + "\"></script>");
			File.WriteAllText ("HTMLAssistant_Data\\output.html", outputWithJS);
		}
	}
	/// <summary>
	/// This functions searched for blocks inside <head> area, adds their text to the temp file, does the same for the body block, adds css and js files, and runs the temp file.
	/// </summary>
	public void renderHTML ()
	{
		UnityEngine.Debug.Log ("Clicked Run button");		
		string result = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"UTF-8\" /><title>Try</title><head>";
		SortedList fieldList = new SortedList ();
		GameObject panel = GameObject.Find ("PanelHead");
		RectTransform rt = panel.GetComponent<RectTransform> ();	
		Vector3 vectorPanel = panel.transform.position;
		for (int i = 0; i < blockFunctions.blocks.Count; i++) {
			if (blockFunctions.blocks [i]) {
				InputField temp = blockFunctions.blocks [i].GetComponent<InputField> ();
				Vector3 vectorTemp = temp.transform.position;
				if (((vectorTemp.x < (vectorPanel.x + 0.5 * rt.sizeDelta.x)) && (vectorTemp.x > (vectorPanel.x - 0.5 * rt.sizeDelta.x))) && ((vectorTemp.y < (vectorPanel.y + 0.5 * rt.sizeDelta.y)) && (vectorTemp.y > (vectorPanel.y - 0.5 * rt.sizeDelta.y))))
					fieldList.Add (vectorTemp.y, temp.text);
			}
		}
		for (int i = fieldList.Count -1; i >= 0; i--) {
			result = result + (fieldList.GetByIndex (i)) + "\n";
		}
		fieldList.Clear ();
		result = result + "</head><body>";
		panel = GameObject.Find ("PanelBody");
		rt = panel.GetComponent<RectTransform> ();	
		vectorPanel = panel.transform.position;
		for (int i = 0; i < blockFunctions.blocks.Count; i++) {
			if (blockFunctions.blocks [i]) {
				InputField temp = blockFunctions.blocks [i].GetComponent<InputField> ();
				Vector3 vectorTemp = temp.transform.position;
				if (((vectorTemp.x < (vectorPanel.x + 0.5 * rt.sizeDelta.x)) && (vectorTemp.x > (vectorPanel.x - 0.5 * rt.sizeDelta.x))) && ((vectorTemp.y < (vectorPanel.y + 0.5 * rt.sizeDelta.y)) && (vectorTemp.y > (vectorPanel.y - 0.5 * rt.sizeDelta.y))))
					fieldList.Add (vectorTemp.y, temp.text);
			}
		}
		for (int i = fieldList.Count -1; i >= 0; i--) {
			result = result + (fieldList.GetByIndex (i)) + "\n";
		}
		fieldList.Clear ();
		result = result + "</body></html>";
		File.WriteAllText ("HTMLAssistant_Data\\output.html", result);
		addCSS ();
		addJS ();
		System.Diagnostics.Process.Start ("HTMLAssistant_Data\\output.html");
	}
}
