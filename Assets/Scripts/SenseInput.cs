﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Reflection;
using System.Diagnostics;
using System.Runtime.InteropServices;

public class SenseInput : MonoBehaviour
{
	public enum GestureEnum
	{
		wave,
		v_sign,
		two_fingers_pinch_open,
		thumb_up,
		thumb_down,
		tap,
		swipe_up,
		swipe_right,
		swipe_left,
		swipe_down,
		spreadfingers,
		full_pinch,
		fist,
		click
	}

	public enum ModeEnum
	{
		MOVE,
		ENDMOVE
	}

	private PXCMSenseManager sm = null;
	private PXCMHandConfiguration handConfig = null;
	private PXCMHandModule hand = null;
	private PXCMHandData handData = null;
	private PXCMTouchlessController tc = null;

	public delegate void OnHandDataDelegate (Vector3 xy);

	public  event OnHandDataDelegate   OnHandData;

	private Vector3 HandPosition;
	public bool moveMode = false;

	void Awake ()
	{
		File.Delete ("HTMLAssistant_Data\\output.html");
		UnityEngine.Debug.Log ("Initialization complete");
	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start ()
	{
		/* Initialize a PXCMSenseManager instance */
		sm = PXCMSenseManager.CreateInstance ();
		if (sm == null)
			return;

		/* Enable hand tracking and configure the hand module */
		sm.EnableHand ();
		hand = sm.QueryHand ();
		if (hand != null) {
			//Configure the hand tracker to track gestures
			handConfig = hand.CreateActiveConfiguration ();
			SetDefaultGestures ();
			handData = hand.CreateOutput ();
		}

		/* Enable touchless controller to enable cursor motion control */
		sm.EnableTouchlessController ();
		tc = sm.QueryTouchlessController ();
		if (tc != null) {
			tc.SubscribeEvent (OnTouchlessControllerUXEvent);
			PXCMTouchlessController.ProfileInfo pinfo = new PXCMTouchlessController.ProfileInfo ();
			pinfo.config = PXCMTouchlessController.ProfileInfo.Configuration.Configuration_Allow_Selection; //This enables the tap gesture
			tc.SetProfile (pinfo);
		}

		/* Initialize the execution pipeline */
		pxcmStatus sts = sm.Init ();
		if (sts < pxcmStatus.PXCM_STATUS_NO_ERROR) {
			OnDisable ();
			return;
		}
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update ()
	{
		if (sm == null)
			return;

		/* Wait until any frame data is available */
		if (sm.AcquireFrame (false, 0) < pxcmStatus.PXCM_STATUS_NO_ERROR)
			return;

		/* Retrieve hand tracking data if ready */
		if (hand != null) {
			handData.Update ();
			//Check gestures to set the mode and/or call block functions
			CheckGestures ();

			OnHandData (HandPosition);
		}

		/* Now, process the next frame */
		sm.ReleaseFrame ();
	}

	/// <summary>
	/// Raises the disable event.
	/// </summary>
	void OnDisable ()
	{
		if (sm == null)
			return;
		sm.Dispose ();
		sm = null;
	}

	/// <summary>
	/// Raises the touchless controller UX event event.
	/// </summary>
	protected void OnTouchlessControllerUXEvent (PXCMTouchlessController.UXEventData e)
	{
		switch (e.type) {
		case PXCMTouchlessController.UXEventData.UXEventType.UXEvent_Select:
			var t = GameObject.Find ("Create");
			var b = t.GetComponent<Button> ();
			b.onClick.Invoke ();
			break;
		case PXCMTouchlessController.UXEventData.UXEventType.UXEvent_CursorMove:
			HandPosition.x = Convert.ToInt32 (e.position.x * Screen.width);
			HandPosition.y = Convert.ToInt32 (e.position.y * Screen.height);
			break;
		}
	}

	/// <summary>
	/// Sets the configuration for the mode that is passed in
	/// </summary>
	/// <param name="mode">Current mode to set configuration for</param>
	public void SetMode (ModeEnum mode)
	{
		switch (mode) {
		case ModeEnum.MOVE:
			handConfig.DisableGesture (GestureEnum.two_fingers_pinch_open.ToString ());
			handConfig.EnableGesture (SenseInput.GestureEnum.spreadfingers.ToString (), false);
			handConfig.ApplyChanges ();
			UnityEngine.Debug.Log ("Move activated");
			moveMode = true;
			break;
		case ModeEnum.ENDMOVE:
			SetDefaultGestures ();
			UnityEngine.Debug.Log ("Move de-activated");
			moveMode = false;
			break;
		}

	}

	/// <summary>
	/// Enable the default gestures
	/// </summary>
	public void SetDefaultGestures ()
	{
		if (handConfig == null)
			return;

		handConfig.DisableGesture (GestureEnum.spreadfingers.ToString ());
		handConfig.EnableGesture (GestureEnum.two_fingers_pinch_open.ToString (), false);//Initiate Move

		handConfig.ApplyChanges ();
	}

	/// <summary>
	/// Checks if a gesture has been fired and sets the appropriate mode
	/// </summary>
	void CheckGestures ()
	{
		PXCMHandData.GestureData data;

		if (handData.IsGestureFired (GestureEnum.two_fingers_pinch_open.ToString (), out data)) {
			if (data.state == PXCMHandData.GestureStateType.GESTURE_STATE_START)
				SetMode (ModeEnum.MOVE);
		} else if (handConfig.IsGestureEnabled (GestureEnum.spreadfingers.ToString ()) && handData.IsGestureFired (GestureEnum.spreadfingers.ToString (), out data)) {
			if (data.state == PXCMHandData.GestureStateType.GESTURE_STATE_START)
				SetMode (ModeEnum.ENDMOVE);
		}
	}
}
